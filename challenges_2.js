/*
Challenge 1: LONGEST WORD
Return the longest word of a string
If more than one longest word, put into an array
ex: longestWord('Hello, my name is Oliver Queen)
*/

// Solution 1
function longestWord(sen) {
  let longestWord = 0;
  const wordArr = sen.split(" ");

  let word = [];
  for (let i = 0; i < wordArr.length; i++) {
    let formattedWord = wordArr[i].replace(/,/g, "").trim();
    if (formattedWord.length >= longestWord) {
      longestWord = formattedWord.length;
      word.push(formattedWord);
    }
  }
  return word.length >= 1 ? word : word[0];
}
// Solution 2
function longestWord2(sen) {
  const wordArr = sen.toLowerCase().match(/[a-z0-9]+/g, "");

  const sorted = wordArr.sort((a, b) => b.length - a.length);

  return sorted.filter(word => word.length === sorted[0].length);
}

// Challenge 2: ARRAY CHUNKING
// Split an array into chunked arrays of specific length

function chunkArray(arr, len) {
  const chunkedArray = [];

  for (let i = 0; i < arr.length; i += len) {
    chunkedArray.push(arr.slice(i, i + len)); // slice returns selected elements as a new array object ^^
  }

  return chunkedArray;
}

// Challenge 3: Flatten array
// Take an array of arrays and flatten to a single array

function flattenArray(arr) {
  return arr.reduce((a, b) => a.concat(b), []);
}

// Challenge 4: ANAGRAM
// Return true if anagram and false if not
// ex: elbow === below

/**
 * 
 * @param {string} str1 
 * @param {string} str2 
 */
function isAnagram(str1, str2) {
 return formatStr(str1) === formatStr(str2);
}

/**
 * @param {string} str
 */
function formatStr(str) { // helper function for isAnagram
  return str
    .replace(/[^\w]/g, "") //will replace any non letter symbols
    .toLowerCase()
    .split("")
    .sort() // sort from ascending -> elbow = below
    .join("");
}

const output = isAnagram('elbow', 'below');
console.log(output);
