/*
Challenge 1:
Return a string in reverse
example: reverseString('hello) === 'olleh'
*/

function reverseString(str) {
  // Solution 1(built in functions):
  /* return str
    .split("")
    .reverse()
    .join("");    */

  //Solution 2(decrementing for):
  /* let reverseString = '';
    for(let i = str.length - 1; i >= 0; i-- ){
        reverseString = reverseString + str[i];
    }
    return reverseString; */

  // Solution 3(for w/o index):
  /* let revString = '';
    for(let char of str){
        revString = char + revString;
    }
    return revString; */

  // Solution 4 (ES6):
  /* let revString = ''; 
    str.split('').forEach(char => revString = char + revString);
    return revString; */

  // Solution 5(reduce/es6):
  return str.split("").reduce((revString, char) => char + revString, "");
}

/*
Challenge 2: Validate palindrome
Return true if palindrome and false if not
example: isPalindrome('racercar') === true; 
*/

// Solution
function isPalindrome(str) {
  const revString = str
    .split("")
    .reverse()
    .join("");

  return revString === str;
}

/*
Challenge 3: Reverse an integer
ex: 623 = 326
*/

// Solution:
function reverseInt(int) {
  const revString = int
    .toString()
    .split("")
    .reverse()
    .join("");
  return Number.parseInt(revString) * Math.sign(int); //sign handles operator symbols
}

/*
Challenge 4: Capitalize letters
Return a string with the first letter of every word capitalized
ex: 'i love javascript' === 'I Love Javascript'
*/

function capitalizeLetters(str) {
  // Solution 1:
  /*
  const strArray = str.toLowerCase().split(" ");
  for (let i = 0; i < strArray.length; i++) {
    strArray[i] =
      strArray[i].substring(0, 1).toUpperCase() + strArray[i].substring(1);
  }
  return strArray.join(" "); */

  // Solution 2:
  return str
    .toLowerCase()
    .split(" ")
    .map(
      word => (word = word.substring(0, 1).toUpperCase() + word.substring(1))
    )
    .join(" ");
}

/*
Challenge 5: Max character
Return the character that is most common in a string
example: 'javascript' == 'a'
*/

function maxChar(str) {
  const charMap = {};
  let maxOccurrences = 0;
  let charOccur = "";

  str.split("").forEach(char => {
    if (charMap[char]) charMap[char]++;
    else charMap[char] = 1;
  });

  for (let char in charMap) {
    if (charMap[char] > maxOccurrences) {
      maxOccurrences = charMap[char];
      charOccur = char;
    }
  }

  return charOccur;
}

// Challenge 6: Shift M elements
// Given an array of integers, shift M number of elements to the end
// <= 0 < m < n

function shiftElements(m) {
  let n = [1, 2, 3, 4, 5];

  /* Solution 1: */
  // for (i = 0; i < m; i++) {
  //   let temp = n[0];
  //   for(j = 0; j < n.length - 1; j++){
  //     console.log(n[j]);
  //     n[j] = n[j + 1];
  //   }
  //   n[j] = temp;
  // }

  // Solution 2:
  for(i = 0; i < m; i++){
    n.push(n.shift());
  }
return n;
}

// Challenge 7: FizzBuzz
/* Write a program that prints all the numbers from 1 to 100. 
For multiples of 3, instead of the number print Fizz, for multiples 
of 5 print "Buzz". For numbers which are multiples of 3 and 5 print FizzBuzz
*/

function fizzBuzz(){
  for(i = 1; i <= 100; i++){
    if(i % 3 === 0 && i % 5 === 0){
      console.log("FizzBuzz");
    } else if (i % 3 === 0){
      console.log("Fizz");
    } else if (i % 5 === 0){
      console.log("Buzz");
    } else {
      console.log(i);
    }
  }
}

const output = fizzBuzz();

console.log(output);
